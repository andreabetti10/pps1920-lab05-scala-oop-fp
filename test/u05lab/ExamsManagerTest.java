package u05lab;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import scala.Option;
import scala.collection.JavaConverters;
import u05lab.code.*;

import java.util.Arrays;
import java.util.HashSet;

public class ExamsManagerTest {

    private ExamsManager em = new ExamsManagerImpl();

    @Test
    public void testExamResultsBasicBehaviour() {
        // esame fallito, non c'è voto
        assertEquals(ExamResult.failed().getKind(), Kind.Failed());
        assertFalse(ExamResult.failed().getEvaluation().isDefined());
        assertFalse(ExamResult.failed().cumLaude());
        assertEquals(ExamResult.failed().toString(), "FAILED");

        // lo studente si è ritirato, non c'è voto
        assertEquals(ExamResult.retired().getKind(), Kind.Retired());
        assertFalse(ExamResult.retired().getEvaluation().isDefined());
        assertFalse(ExamResult.retired().cumLaude());
        assertEquals(ExamResult.retired().toString(), "RETIRED");

        // 30L
        assertEquals(ExamResult.succeededCumLaude().getKind(), Kind.Succeded());
        assertEquals(ExamResult.succeededCumLaude().getEvaluation(), Option.apply(30));
        assertTrue(ExamResult.succeededCumLaude().cumLaude());
        assertEquals(ExamResult.succeededCumLaude().toString(), "SUCCEEDED(30L)");

        // esame superato, ma non con lode
        assertEquals(ExamResult.succeeded(28).getKind(), Kind.Succeded());
        assertEquals(ExamResult.succeeded(28).getEvaluation(), Option.apply(28));
        assertFalse(ExamResult.succeeded(28).cumLaude());
        assertEquals(ExamResult.succeeded(28).toString(), "SUCCEEDED(28)");
    }

    @Test
    public void optionalTestEvaluationCantBeGreaterThan30() {
        assertThrows(IllegalArgumentException.class, () -> ExamResult.succeeded(32));
    }

    @Test
    public void optionalTestEvaluationCantBeSmallerThan18() {
        assertThrows(IllegalArgumentException.class, () -> ExamResult.succeeded(17));
    }

    private void prepareExams() {
        em.createNewCall("gennaio");
        em.createNewCall("febbraio");
        em.createNewCall("marzo");

        em.addStudentResult("gennaio", "rossi", ExamResult.failed()); // rossi -> fallito
        em.addStudentResult("gennaio", "bianchi", ExamResult.retired()); // bianchi -> ritirato
        em.addStudentResult("gennaio", "verdi", ExamResult.succeeded(28)); // verdi -> 28
        em.addStudentResult("gennaio", "neri", ExamResult.succeededCumLaude()); // neri -> 30L

        em.addStudentResult("febbraio", "rossi", ExamResult.failed()); // etc..
        em.addStudentResult("febbraio", "bianchi", ExamResult.succeeded(20));
        em.addStudentResult("febbraio", "verdi", ExamResult.succeeded(30));

        em.addStudentResult("marzo", "rossi", ExamResult.succeeded(25));
        em.addStudentResult("marzo", "bianchi", ExamResult.succeeded(25));
        em.addStudentResult("marzo", "viola", ExamResult.failed());
    }

    @Test
    public void testExamsManagement() {
        this.prepareExams();
        // partecipanti agli appelli di gennaio e marzo
        assertEquals(em.getAllStudentsFromCall("gennaio"), JavaConverters.asScalaSet(new HashSet<>(Arrays.asList("rossi","bianchi","verdi","neri"))));
        assertEquals(em.getAllStudentsFromCall("marzo"),  JavaConverters.asScalaSet(new HashSet<>(Arrays.asList("rossi","bianchi","viola"))));

        // promossi di gennaio con voto
        assertEquals(em.getEvaluationsMapFromCall("gennaio").size(),2);
        assertEquals(em.getEvaluationsMapFromCall("gennaio").get("verdi").get(),28);
        assertEquals(em.getEvaluationsMapFromCall("gennaio").get("neri").get(),30);
        // promossi di febbraio con voto
        assertEquals(em.getEvaluationsMapFromCall("febbraio").size(),2);
        assertEquals(em.getEvaluationsMapFromCall("febbraio").get("bianchi").get(),20);
        assertEquals(em.getEvaluationsMapFromCall("febbraio").get("verdi").get(),30);

        // tutti i risultati di rossi (attenzione ai toString!!)
        assertEquals(em.getResultsMapFromStudent("rossi").size(),3);
        assertEquals(em.getResultsMapFromStudent("rossi").get("gennaio").get(),"FAILED");
        assertEquals(em.getResultsMapFromStudent("rossi").get("febbraio").get(),"FAILED");
        assertEquals(em.getResultsMapFromStudent("rossi").get("marzo").get(),"SUCCEEDED(25)");
        // tutti i risultati di bianchi
        assertEquals(em.getResultsMapFromStudent("bianchi").size(),3);
        assertEquals(em.getResultsMapFromStudent("bianchi").get("gennaio").get(),"RETIRED");
        assertEquals(em.getResultsMapFromStudent("bianchi").get("febbraio").get(),"SUCCEEDED(20)");
        assertEquals(em.getResultsMapFromStudent("bianchi").get("marzo").get(),"SUCCEEDED(25)");
        // tutti i risultati di neri
        assertEquals(em.getResultsMapFromStudent("neri").size(),1);
        assertEquals(em.getResultsMapFromStudent("neri").get("gennaio").get(),"SUCCEEDED(30L)");

    }

    @Test
    public void optionalTestExamsManagement() {
        this.prepareExams();
        // miglior voto acquisito da ogni studente, o vuoto..
        assertEquals(em.getBestResultFromStudent("rossi"), Option.apply(25));
        assertEquals(em.getBestResultFromStudent("bianchi"), Option.apply(25));
        assertEquals(em.getBestResultFromStudent("neri"), Option.apply(30));
        assertEquals(em.getBestResultFromStudent("viola"), Option.empty());
    }

    @Test
    public void optionalTestCantCreateACallTwice() {
        this.prepareExams();
        assertThrows(IllegalArgumentException.class, () -> em.createNewCall("marzo"));
    }

    @Test
    public void optionalTestCantRegisterAnEvaluationTwice() {
        this.prepareExams();
        assertThrows(IllegalArgumentException.class, () -> em.addStudentResult("gennaio", "verdi", ExamResult.failed()));
    }

}