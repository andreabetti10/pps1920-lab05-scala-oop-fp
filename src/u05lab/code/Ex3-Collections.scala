package u05lab.code

import java.util.concurrent.TimeUnit

import scala.collection.mutable.{ArrayBuffer, ListBuffer}
import scala.concurrent.duration.FiniteDuration

object PerformanceUtils {
  case class MeasurementResults[T](result: T, duration: FiniteDuration) extends Ordered[MeasurementResults[_]] {
    override def compare(that: MeasurementResults[_]): Int = duration.toNanos.compareTo(that.duration.toNanos)
  }

  def measure[T](msg: String)(expr: => T): MeasurementResults[T] = {
    val startTime = System.nanoTime()
    val res = expr
    val duration = FiniteDuration(System.nanoTime()-startTime, TimeUnit.NANOSECONDS)
    if(!msg.isEmpty) println(msg + " -- " + duration.toNanos + " nanos; " + duration.toMillis + "ms")
    MeasurementResults(res, duration)
  }

  def measure[T](expr: => T): MeasurementResults[T] = measure("")(expr)
}


object CollectionsTest extends App {

  /* Linear sequences: List, ListBuffer */
  var l : List[Int] = List(1, 2, 3)
  println(l)
  println(l.length) // not modifiable list
  println(l :+ 4)
  println((l :+ 4).length)

  var lb : ListBuffer[Int] = ListBuffer(1, 2, 3)
  println(lb)
  println(lb.length)
  lb += 2
  println(lb)
  println(lb.length)
  println(lb :+ 4)
  println((lb :+ 4).length)
  lb -= 2
  println(lb)
  println(lb.length)

  /* Indexed sequences: Vector, Array, ArrayBuffer */
  var vector: Vector[Int] = Vector(1, 2, 3, 4, 5)
  println(vector)
  println(vector.length) // not modifiable vector
  println(vector :+ 4)
  println((vector :+ 4).length)

  var array: Array[Int] = Array(1, 2, 3, 4, 5)
  println(array.mkString(" "))
  println(array.length)
  array.update(2, 6)
  println(array.mkString(" "))
  println(array.length)

  var arrayBuffer: ArrayBuffer[Int] = ArrayBuffer(1, 2, 3, 4, 5)
  println(arrayBuffer)
  println(arrayBuffer.length)
  arrayBuffer += 2
  println(arrayBuffer)
  println(arrayBuffer.length)
  println(arrayBuffer :+ 4)
  println((arrayBuffer :+ 4).length)
  arrayBuffer -= 2
  println(arrayBuffer)
  println(arrayBuffer.length)
  arrayBuffer.update(1, 7)
  println(arrayBuffer)
  println(arrayBuffer.length)

  /* Sets */
  var set: Set[Int] = Set(1, 2, 3, 4, 5)
  println(set)
  println(set.size)
  set += 2
  println(set)
  println(set.size)
  set += 6
  println(set)
  println(set.size)
  set -= 2
  println(set)
  println(set.size)

  var mset: scala.collection.mutable.Set[Int] = scala.collection.mutable.Set(1, 2, 3, 4, 5)
  println(mset)
  println(mset.size)
  mset += 2
  println(mset)
  println(mset.size)
  mset += 6
  println(mset)
  println(mset.size)
  mset -= 2
  println(mset)
  println(mset.size)
  mset.update(6, true)
  println(mset)
  println(mset.size)
  mset.update(20, true)
  println(mset)
  println(mset.size)
  mset.update(20, false)
  println(mset)
  println(mset.size)

  /* Maps */
  var map: Map[Int, String] = Map(10 -> "a", 20 -> "b", 30 -> "c")
  println(map)
  println(map.size)
  map += 2 -> "d"
  println(map)
  println(map.size)
  map -= 2
  println(map)
  println(map.size)

  var mmap: scala.collection.mutable.Map[Int, String] = scala.collection.mutable.Map(10 -> "a", 20 -> "b", 30 -> "c")
  println(mmap)
  println(mmap.size)
  mmap += 2 -> "d"
  println(mmap)
  println(mmap.size)
  mmap -= 2
  println(mmap)
  println(mmap.size)
  mmap.update(10, "x")
  println(mmap)
  println(mmap.size)

  /* Comparison */
  import PerformanceUtils._
  val lst = (1 to 1000000).toList
  val vec = (1 to 1000000).toVector
  assert( measure("lst last"){ lst.last } > measure("vec last"){ vec.last } )
}