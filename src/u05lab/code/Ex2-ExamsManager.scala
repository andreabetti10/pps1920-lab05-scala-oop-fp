package u05lab.code

import scala.collection.immutable.HashMap

object Kind extends Enumeration {
  val Retired, Failed, Succeded = Value
}

sealed trait ExamResult {

  def getKind(): Kind.Value

  def getEvaluation(): Option[Int]

  def cumLaude(): Boolean

}

case class Failed() extends ExamResultImpl {
  override def toString(): String = "FAILED"
}

case class Retired() extends ExamResultImpl {
  override def toString(): String = "RETIRED"
}

case class SucceededCumLaude() extends ExamResultImpl {
  override def toString(): String = "SUCCEEDED(30L)"
}

case class Succeeded(evaluation: Int) extends ExamResultImpl {
  override def toString(): String = "SUCCEEDED("+evaluation+")"
}

trait ExamResultImpl extends ExamResult {

  override def getKind(): Kind.Value = this match {
    case Failed() => Kind.Failed
    case Retired() => Kind.Retired
    case Succeeded(_) | SucceededCumLaude() => Kind.Succeded
    case _ => throw new IllegalArgumentException()
  };

  override def getEvaluation(): Option[Int] = this match {
    case Succeeded(evaluation) => Some(evaluation)
    case SucceededCumLaude() => Some(30)
    case _ => None
  }

  override def cumLaude(): Boolean = this match {
    case SucceededCumLaude() => true
    case _ => false
  }
}

object ExamResult {

  def failed: ExamResult = Failed()
  def retired: ExamResult = Retired()
  def succeededCumLaude: ExamResult = SucceededCumLaude()
  def succeeded(evaluation: Int): ExamResult = {
    if (evaluation > 30 || evaluation < 18)
      throw new IllegalArgumentException()
    else Succeeded(evaluation)
  }

}

sealed trait ExamsManager {

  def createNewCall(call: String): Unit

  def addStudentResult(call: String, student: String, examResult: ExamResult): Unit

  def getAllStudentsFromCall(call: String): Set[String]

  def getEvaluationsMapFromCall(call: String): Map[String, Int]

  def getResultsMapFromStudent(student: String): Map[String, String]

  def getBestResultFromStudent(student: String): Option[Int]

}

object ExamsManager {

  def apply(): ExamsManager = ExamsManagerImpl();

}

case class ExamsManagerImpl() extends ExamsManager {

  private var map : Map[String, Map[String, ExamResult]] = new HashMap()

  private def checkArgument(condition: Boolean): Unit = {
    if (!condition) throw new IllegalArgumentException
  }

  override def createNewCall(call: String): Unit = {
    checkArgument(!(map contains call))
    map += call -> HashMap()
  }

  override def addStudentResult(call: String, student: String, examResult: ExamResult): Unit = {
    checkArgument(map contains call)
    checkArgument(!(map(call) contains student))
    map += (call -> (map(call) + (student -> examResult)))
  }

  override def getAllStudentsFromCall(call: String): Set[String] = {
    checkArgument(map contains call)
    map(call).keySet
  }

  override def getEvaluationsMapFromCall(call: String): Map[String, Int] = {
    checkArgument(map contains call)
    map(call).filter(_._2.getEvaluation().isDefined).mapValues(_.getEvaluation().get)
  }

  override def getResultsMapFromStudent(student: String): Map[String, String] =
    map.filter(_._2.contains(student)).mapValues(_.get(student).get.toString)

  override def getBestResultFromStudent(student: String): Option[Int] =
    map.values.toSeq.flatMap(_.toSeq)
      .filter(_._1 == student)
      .map(_._2)
      .filter(_.getEvaluation().isDefined)
      .map(_.getEvaluation().get)
      .reduceOption(math.max)
}

